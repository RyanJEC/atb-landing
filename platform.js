if(navigator.userAgent.match('iPhone') || navigator.userAgent.match('iPad') || navigator.userAgent.match('iPod') || navigator.userAgent.match('Android') || navigator.userAgent.match('Mobile') || navigator.userAgent.match('Tablet') || navigator.userAgent.match('Phone')){
	
	$('<meta name="viewport" content="height=device-height, width=device-width, initial-scale=1.0, maximum-scale=1">').appendTo('head');
	$('<link rel="stylesheet" type="text/css" href="mobile.css">').appendTo('head');
	$('<body></body>').appendTo('html');
	$('body').load('mobile.html');
}else{
	$('<link rel="stylesheet" type="text/css" href="desktop.css">').appendTo('head');
	$('<body></body>').appendTo('html');
	$('body').load('desktop.html');
}


let streams = {
	twitch: 'Twitch',
	twitch_link: 'https://twitch.tv/bwana',
	twitch_stream: 'https://player.twitch.tv/?channel=bwana',
	twitch_chat: 'http://www.twitch.tv/bwana/chat',
	mixer: 'Mixer',
	mixer_link: 'https://mixer.com/bwana',
	mixer_stream: 'https://mixer.com/embed/player/bwana',
	mixer_chat: 'https://mixer.com/embed/chat/bwana',
	youtube: 'YouTube',
	youtube_link: 'https://gaming.youtube.com/c/bwana/live',
	youtube_stream: 'https://gaming.youtube.com/embed/live_stream?channel=UCiQO7vpveX6KSzDsINR4Xjw&autoplay=1',
	youtube_chat: 'https://gaming.youtube.com/live_chat?v=BvpWl-HglSI&embed_domain=' + location.hostname
}