$('.page').css({
	width: window.innerWidth,
	height: window.innerHeight
})

/*$('.stream-embed').attr({
	width: window.innerWidth,
	height: window.innerWidth * 9/16
})

$('.chat-embed').attr({
	width: window.innerWidth,
	height: window.innerHeight - 40 - $('.stream-embed').attr('height')
})*/

$(window).on('deviceorientation', function(e) {
	$('.page').css({
		width: window.innerWidth,
		height: window.innerHeight
	})/*
	
	$('.stream-embed').attr({
		width: window.innerWidth,
		height: window.innerWidth * 9 / 16
	})

	$('.chat-embed').attr({
		width: window.innerWidth,
		height: window.innerHeight - 40 - $('.stream-embed').attr('height')
	})*/
});

$('.content').delegate('.stream-button', 'click', function(e) {
	if (!$(this).hasClass('selected')) {
		let data = $(this).attr('data');
		$('.stream-embed').attr('src', streams[data + '_stream']);
		$('.watch-on').attr('href', streams[data + '_link']);
		$('.service').text(streams[data]);
		$('.stream-tab').removeClass('selected');
		$(this).addClass('selected');
	}
});